#!/bin/bash

# 定义步骤函数
function step_ping_and_connect_wifi() {
    echo "1) Ping 网络并连接 WiFi"
    if ping -c 4 google.com > /dev/null; then
        echo "网络连接正常，ping 成功。"
    else
        echo "无法 ping google.com，尝试连接 WiFi..."
        step_connect_wifi
    fi
}

function step_connect_wifi() {
    echo "执行联网"
    sudo nmcli device wifi connect "ndyjy" password "ndyjy123456"
}

function step_system_info() {
    echo "2) 查看系统信息"
    head -n 1 /etc/nv_tegra_release  # 查看L4T版本
    cat /etc/lsb-release             # 查看操作系统版本
    uname -a                         # 查看内核驱动版本
    sudo apt-cache show nvidia-jetpack # 查看JetPack版本
    df -h                            # 查看磁盘空间占用
}

function step_install_tools() {
    echo "3) 安装必要工具"
    sudo apt install nano ufw lsof cutecom -y
}

function step_firewall() {
    echo "4) 防火墙"
    sudo ufw disable
    sudo systemctl status ssh
    sudo iptables -L -v -n
}

function step_config_ssh() {
    echo "5) 配置ssh端口&&开放必要端口"
    
    # 检查SSH配置是否已存在
    if ! sudo grep -q "Port 22" /etc/ssh/sshd_config; then
        sudo sed -i 's/#Port 22/Port 22/' /etc/ssh/sshd_config
        echo "SSH端口配置已添加。"
    else
        echo "SSH端口配置已存在，无需添加。"
    fi

    if ! sudo grep -q "PermitRootLogin yes" /etc/ssh/sshd_config; then
        sudo sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config
        echo "PermitRootLogin配置已添加。"
    else
        echo "PermitRootLogin配置已存在，无需添加。"
    fi

    sudo systemctl restart ssh
    sudo iptables -A INPUT -p udp --dport 9091 -j ACCEPT
    sudo iptables-save | sudo tee /etc/iptables/rules.v4
}

function step_python_env() {
    echo "6) Python环境监测&安装必要库"
    python --version
    python3 --version
    pip --version
    pip3 --version
    sudo apt-get install python python3.7 python-pip python3-pip -y
    sudo pip3 install catkin_pkg rospkg -U jetson-stats
    export TERM=xterm-256color
}

function step_install_ros() {
    echo "7) 一键安装ROS"
    wget http://fishros.com/install -O fishros && . fishros
}

function step_install_ros_libs() {
    echo "8) 安装ROS必要的安装库"
    sudo apt-get install pkg-config ros-melodic-serial* ros-melodic-uuid* ros-melodic-bfl -y
    sudo apt-get install ros-melodic-mbf-costmap-core ros-melodic-costmap* ros-melodic-gmapping* ros-melodic-hector* ros-melodic-slam-karto* ros-melodic-joy -y
    sudo apt-get install python-sklear* python-igraph libceres-dev libudev-dev libdw-dev libsdl1.2-dev libsdl2-dev libpcap-dev libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev libsdl-image1.2-dev -y
}

function step_mount_usb() {
    echo "9) 插入U盘，查看磁盘挂载，复制源码，显示复制进度，并结束安全卸载"
    sudo fdisk -l
    sudo umount /mnt/usb  # 确保U盘已安全卸载
    sudo mount /dev/sdb1 /mnt/usb
    sudo mount -o remount,rw /mnt/usb
    sudo tar -xf /mnt/usb/archive.tar --no-same-owner --no-same-permissions -C /home/nano/Desktop/ ros_workspace
}

function step_backup_to_usb() {
    echo "15) 备份源码到U盘"
    sudo fdisk -l
    sudo umount /mnt/usb  # 确保U盘已安全卸载
    sudo mount /dev/sdb1 /mnt/usb
    sudo mount -o remount,rw /mnt/usb
    sudo tar -cf /mnt/usb/archive.tar --no-same-owner --no-same-permissions -C /home/nano/Desktop/ ros_workspace
    echo "备份完成，已保存到U盘。"
}

function step_config_workspace() {
    echo "10) 源码空间配置"
    cd /home/nano/Desktop/ros_workspace
    source /opt/ros/melodic/setup.bash
    catkin init
    catkin config --extend /opt/ros/melodic
    catkin config --merge-devel
    sudo chmod -R 777 *
    sudo rsync -aq --info=progress2 --no-owner --no-group /home/nano/Desktop/ros_workspace /mnt/usb/
}

function step_compile() {
    echo "11) 编译"
    rosdep install --from-paths src --ignore-src -r -y
    rm -rf ~/Desktop/ros_workspace/build ~/Desktop/ros_workspace/devel
    catkin build -DCMAKE_BUILD_TYPE=Release -j4
}

function step_config_permissions() {
    echo "12) 配置权限"
    
    # 检查权限配置是否已存在
    if ! sudo grep -q "nano ALL=(ALL) NOPASSWD: /home/nano/Desktop/ros_workspace/src/turn_on_wheeltec_robot/scripts/kill.sh" /etc/sudoers; then
        echo "nano ALL=(ALL) NOPASSWD: /home/nano/Desktop/ros_workspace/src/turn_on_wheeltec_robot/scripts/kill.sh" | sudo tee -a /etc/sudoers
        echo "权限配置已添加。"
    else
        echo "权限配置已存在，无需添加。"
    fi
}

function step_config_environment() {
    echo "13) 配置运行环境"
    
    # 检查配置是否已存在
    if ! grep -q "source /opt/ros/melodic/setup.bash" ~/.bashrc; then
        {
            echo "source /opt/ros/melodic/setup.bash"
            echo "export PYTHONIOENCODING=utf-8"
            echo "source /home/nano/Desktop/ros_workspace/devel/setup.bash"
            echo "catkin_make() { (cd /home/nano/Desktop/ros_workspace/ && command catkin_make \"\$@\"); }"
            echo "catkin() { (cd /home/nano/Desktop/ros_workspace/ && command catkin build \"\$@\"); }"
        } >> ~/.bashrc
        echo "配置已添加到 ~/.bashrc"
    else
        echo "配置已存在，无需添加。"
    fi

    # 提示用户刷新终端
    echo "请运行 'source ~/.bashrc' 或重新打开终端以应用更改。"
}

function step_config_serial_permissions() {
    echo "14) 连接串口，配置串口权限"
    
    # 检查串口权限配置是否已存在
    if ! groups $USER | grep -q "dialout"; then
        sudo usermod -aG dialout $USER
        echo "用户已添加到 dialout 组。"
    else
        echo "用户已在 dialout 组中，无需添加。"
    fi

    sudo chmod 777 /dev/ttyACM0
}

# 主菜单
while true; do
    echo "请选择要执行的步骤:"
    echo "1) Ping 网络并连接 WiFi"
    echo "2) 查看系统信息"
    echo "3) 安装必要工具"
    echo "4) 防火墙设置"
    echo "5) 配置 SSH"
    echo "6) Python 环境监测"
    echo "7) 一键安装 ROS"
    echo "8) 安装 ROS 必要库"
    echo "9) 挂载 U 盘"
    echo "10) 源码空间配置"
    echo "11) 编译"
    echo "12) 配置权限"
    echo "13) 配置运行环境"
    echo "14) 连接串口，配置串口权限"
    echo "15) 备份源码到U盘"
    echo "0) 退出"
    read -p "输入选项: " option

    case $option in
        1) step_ping_and_connect_wifi ;;
        2) step_system_info ;;
        3) step_install_tools ;;
        4) step_firewall ;;
        5) step_config_ssh ;;
        6) step_python_env ;;
        7) step_install_ros ;;
        8) step_install_ros_libs ;;
        9) step_mount_usb ;;
        10) step_config_workspace ;;
        11) step_compile ;;
        12) step_config_permissions ;;
        13) step_config_environment ;;
        14) step_config_serial_permissions ;;
        15) step_backup_to_usb ;;
        0) echo "退出"; exit ;;
        *) echo "无效选项，请重试." ;;
    esac
done
